Name:		kde4-fedora
Version:	1.0
Release:	5%{?dist}
Summary:	KDE4-Fedora metapackage

License:	GPLv2+	
URL:		https://copr.fedoraproject.org/coprs/peem/kde4-fedora/

Requires:	kde-workspace >= 3:4.0
Requires:	apper >= 3:0.9.1
Requires:	kde-runtime >= 3:15.08.1
Requires:	kdeplasma-addons >= 3:4.14.3
Conflicts:	kdeplasma-addons < 3:4.14
Requires:	kde-plasma-nm >= 2:0.9.3.5
Requires:	sddm-kcm >= 2:0-0.5

%description
A metapackage that install all KDE4 packages required to get 
complete desktop experience on Fedora 22+.

%prep
%setup -c -T

%install

%post
sed -i "s/Current=.*/Current=/" /etc/sddm.conf

%files

%changelog
* Fri Sep 16 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 1.0-5
- Added Conflicts: kdeplasma-addons < 3:4.14 moved from kde-workspace

* Fri Aug 26 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 1.0-4
- dropped some version minor parts
- corrected operators

* Wed Mar 2 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 1.0-3
- fixed version of required sddm-kcm package

* Tue Jan 12 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 1.0-2
- added two more requires.
- added %post action for sddm.conf file

* Thu Nov 26 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 1.0-1
- added metapackage to the repo.
