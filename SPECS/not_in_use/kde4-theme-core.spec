%global parent_name plasma-workspace
Name:    kde4-theme-core
Summary: Core and Inherited theme elements
Version: 5.8.5
Release: 1%{?dist}
BuildArch: noarch

License: GPLv2+
URL:     https://cgit.kde.org/?p=%{parent_name}.git

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: http://download.kde.org/%{stable}/plasma/%{version}/%{parent_name}-%{version}.tar.xz

# This goes to PAM
# TODO: this should arguably be in kde-settings with the other pam-related configs
Source10:       kde
# Desktop file for Fedora look-and-feel packages
Source12:       twenty.two.desktop
Source13:       twenty.three.desktop
Source14:       fedora.desktop

# Default look-and-feel theme
%global         kde4_theme_core 1
%if 0%{?fedora} > 21
Provides:       f22-kde-theme-core = %{version}-%{release}
%endif
%if 0%{?fedora} > 22
Provides:       f23-kde-theme-core = %{version}-%{release}
%endif
%if 0%{?fedora} == 22
Requires:       f22-kde-theme >= 22.2
%global default_lookandfeel org.fedoraproject.fedora.twenty.two
%endif
%if 0%{?fedora} == 23
Requires:       f23-kde-theme
%global default_lookandfeel org.fedoraproject.fedora.twenty.three
%endif
%if 0%{?fedora} == 24
Requires:       f24-kde-theme-core = %{version}-%{release}
%global         f24_kde_theme_core 1
%global default_lookandfeel org.fedoraproject.fedora.twenty.four
%endif
%if 0%{?fedora} > 24
%global         f24_kde_theme_core 1
Requires:       plasma-lookandfeel-fedora = %{version}-%{release}
%global default_lookandfeel org.fedoraproject.fedora.desktop
%endif
%if ! 0%{?default_lookandfeel:1}
Requires:       desktop-backgrounds-compat
%endif

# try use f24-kde-theme etc ...
#Requires: kde4-theme
Provides: system-kde4-plasma-theme = %{fedora}.0
Provides: system-kde4-ksplash-theme = %{fedora}.0
Provides: system-kde4-kdm-theme = %{fedora}.0

%description
%{summary}.

%package -n kde4-sddm-breeze
Summary:        SDDM breeze theme
BuildArch: noarch
%description -n kde4-sddm-breeze
%{summary}.

%package -n f24-kde-theme-core
Summary:  Core and Inherited theme elements
Requires: %{name} = %{version}-%{release}
# when switched to noarch
Obsoletes: f24-kde-theme-core < 5.8.0-5
Requires: f24-kde-theme
BuildArch: noarch
%description -n f24-kde-theme-core
%{summary}.

%package -n plasma-lookandfeel-fedora
Summary:  Fedora look-and-feel for Plasma
Requires: %{name} = %{version}-%{release}
# when switched to noarch
Obsoletes: plasma-lookandfeel-fedora < 5.8.0-5
BuildArch: noarch
%description -n plasma-lookandfeel-fedora
%{summary}.

%prep
%setup -q -n %{parent_name}-%{version}

%build

%install
mkdir -p %{buildroot}%{_datadir}/plasma/look-and-feel
cp -r lookandfeel %{buildroot}%{_datadir}/plasma/look-and-feel/org.kde.breeze.desktop

mkdir -p %{buildroot}%{_datadir}/sddm/themes/breeze
cp -r sddm-theme/* %{buildroot}%{_datadir}/sddm/themes/breeze
rm %{buildroot}%{_datadir}/sddm/themes/breeze/components
cp -r lookandfeel/contents/components %{buildroot}%{_datadir}/sddm/themes/breeze/

%if 0%{?kde4_theme_core}
# Create Fedora Twenty Three look and feel package from the Breeze one
cp -r %{buildroot}%{_datadir}/plasma/look-and-feel/{org.kde.breeze.desktop,org.fedoraproject.fedora.twenty.two}
install -m 0644 %{SOURCE12} %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.two/metadata.desktop
#find %{buildroot} -type d
#/usr/share/kde4/services/
#install -m 0644 %{SOURCE12} %{buildroot}%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.two.desktop
## We need to remove original background which will be replaced by Fedora one from f22-kde-theme
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.two/contents/components/artwork/background.png
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.two/contents/previews/{lockscreen.png,preview.png,splash.png}
%endif

%if 0%{?fedora} > 22
# Create Fedora Twenty Three look and feel package from the Breeze one
cp -r %{buildroot}%{_datadir}/plasma/look-and-feel/{org.kde.breeze.desktop,org.fedoraproject.fedora.twenty.three}
install -m 0644 %{SOURCE13} %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/metadata.desktop
#install -m 0644 %{SOURCE13} %{buildroot}%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.twenty.three.desktop
## We need to remove original background which will be replaced by Fedora one from f23-kde-theme
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/contents/components/artwork/background.png
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.three/contents/previews/{lockscreen.png,preview.png,splash.png}
%endif

%if 0%{?f24_kde_theme_core}
# Create Fedora Twenty Four look and feel package from the Breeze one
cp -r %{buildroot}%{_datadir}/plasma/look-and-feel/{org.kde.breeze.desktop,org.fedoraproject.fedora.twenty.four}
# remove items that will be provided by f24-kde-theme
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/metadata.desktop
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/contents/components/artwork/background.png
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/contents/previews/{lockscreen.png,preview.png,splash.png}
%endif

%if 0%{?fedora} > 24
# Create Fedora look and feel package (
cp -alf %{buildroot}%{_datadir}/plasma/look-and-feel/{org.kde.breeze.desktop,org.fedoraproject.fedora.desktop}
# remove items to be customized
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.desktop/metadata.desktop
rm -fv %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.desktop/contents/components/artwork/background.png
install -m 0644 %{SOURCE14} %{buildroot}%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.desktop/metadata.desktop
#install -m 0644 %{SOURCE14} %{buildroot}%{_datadir}/kservices5/plasma-lookandfeel-org.fedoraproject.fedora.desktop
%endif

# make fedora-breeze sddm theme variant.  FIXME/TODO: corrected preview screenshot
cp -alf %{buildroot}%{_datadir}/sddm/themes/breeze/ \
        %{buildroot}%{_datadir}/sddm/themes/01-breeze-fedora
ln -sf  %{_datadir}/backgrounds/default.png \
        %{buildroot}%{_datadir}/sddm/themes/01-breeze-fedora/components/artwork/background.png
rm -fv  %{buildroot}%{_datadir}/sddm/themes/01-breeze-fedora/theme.conf
cp -a   %{buildroot}%{_datadir}/sddm/themes/breeze/theme.conf \
        %{buildroot}%{_datadir}/sddm/themes/01-breeze-fedora/
sed -i \
  -e "s|background=.*|background=/usr/share/backgrounds/default.png|" \
  %{buildroot}%{_datadir}/sddm/themes/01-breeze-fedora/theme.conf

# Make kcheckpass work
install -m644 -p -D %{SOURCE10} %{buildroot}%{_sysconfdir}/pam.d/kde

%files -n kde4-sddm-breeze
%{_datadir}/sddm/themes/breeze/
%{_datadir}/sddm/themes/01-breeze-fedora/

%if 0%{?kde4_theme_core}
%files
# PAM
%config(noreplace) %{_sysconfdir}/pam.d/kde
%{_datadir}/plasma/look-and-feel/
%endif


%if 0%{?f24_kde_theme_core}
%files -n f24-kde-theme-core
%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.twenty.four/
%endif

%if 0%{?fedora} > 24
%files -n plasma-lookandfeel-fedora
%{_datadir}/plasma/look-and-feel/org.fedoraproject.fedora.desktop/
%endif

%changelog
* Wed Feb 08 2017 Sérgio Basto <sergio@serjux.com> - 5.8.5-1
- Update kde4-theme-core to 5.8.5 as Fedora proper

* Thu Sep 15 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> - 5.7.5-1
- Initial version of package for kde4-fedora repo. split from plasma-workspace
