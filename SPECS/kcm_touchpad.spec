
Name:    kcm_touchpad
Summary: KDE Systemsettings module for touchpads
Version: 1.1
Release: 4%{?dist}

License: GPLv2+
Url:     https://projects.kde.org/kcm-touchpad
 
# use releaseme script to generate tarball, and host ourselves
# at least until we can poke/bribe upstream into doing so 
Source0: http://rdieter.fedorapeople.org/kcm-touchpad/kcm-touchpad-%{version}.tar.xz

BuildRequires: gettext
BuildRequires: kdelibs4-devel
BuildRequires: pkgconfig(x11-xcb)
BuildRequires: pkgconfig(xcb-record)
BuildRequires: pkgconfig(xorg-server)
BuildRequires: pkgconfig(xorg-synaptics)

# kcmshell4
Requires: kde-runtime%{?_kde4_version: >= %{_kde4_version}}


%description
%{summary}.


%prep
%setup -q -n kcm-touchpad-%{version}


%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags}  -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot}  -C %{_target_platform}

%find_lang kcm_touchpad
%find_lang plasma_applet_touchpad
cat *.lang > all.lang


%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi


%files -f all.lang
%doc COPYING
%{_kde4_bindir}/kcm-touchpad-list-devices
%{_kde4_libdir}/kde4/kded_touchpad.so
%{_kde4_libdir}/kde4/plasma_engine_touchpad.so
%{_kde4_datadir}/config.kcfg/touchpad.kcfg
%{_kde4_datadir}/config.kcfg/touchpaddaemon.kcfg
%{_datadir}/dbus-1/interfaces/org.kde.touchpad.xml
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_appsdir}/desktoptheme/default/icons/touchpad.svg
%{_kde4_appsdir}/kcm_touchpad/kcm_touchpad.notifyrc
%{_kde4_appsdir}/plasma/plasmoids/touchpad/
%{_kde4_appsdir}/plasma/services/touchpad.operations
%{_kde4_datadir}/kde4/services/kcm_touchpad.desktop
%{_kde4_datadir}/kde4/services/kded/touchpad.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-touchpad.desktop
%{_kde4_datadir}/kde4/services/plasma-dataengine-touchpad.desktop


%changelog
* Wed Jan 13 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> - 1.1-4
- Initial stable release for kde4-fedora repo

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed May 28 2014 Rex Dieter <rdieter@fedoraproject.org> 1.1-1
- kcm-toucpad-1.1

* Wed May 28 2014 Rex Dieter <rdieter@fedoraproject.org> 1.0-2
- rename to kcm_touchpad

* Thu Apr 03 2014 Rex Dieter <rdieter@fedoraproject.org> 1.0-1
- kcm-touchpad-1.0

* Wed Dec 04 2013 Rex Dieter <rdieter@fedoraproject.org> 0-1.20131204
- first try, 20131204 git snapshot

