
# Fedora package review: http://bugzilla.redhat.com/530342

Summary: Configure the appearance of GTK apps in KDE 
Name:    kcm-gtk 
Version: 0.5.3
Release: 21%{?dist}
Epoch:  1

License: GPLv2+
URL:     https://launchpad.net/kcm-gtk 
Source0: http://launchpad.net/kcm-gtk/0.5.x/%{version}/+download/kcm-gtk_%{version}.orig.tar.gz 

## upstreamable patches
# fix so it appears in systemsettings, not Lost+Found
Patch50: kcm-gtk-0.5.3-settings_category.patch
# ensures GTK2_RC_FILES gets used/updated on first use, avoids 
# possible need for logout/login, code borrowed from kdebase-workspace's krdb.cpp
Patch51: kcm-gtk-0.5.3-gtkrc_setenv.patch
# fix missing umlauts and sharp s in the German translation
# The translations need a lot more fixing than that, but this looks very broken!
Patch52: kcm-gtk-0.5.3-fix-de.patch
# http://bazaar.launchpad.net/~ubuntu-branches/ubuntu/precise/kcm-gtk/precise/view/head:/debian/patches/kubuntu_01_xsettings_kipc.patch
Patch53: kubuntu_01_xsettings_kipc.patch

## upstream patches

BuildRequires: gettext
BuildRequires: kdelibs4-devel >= 4.8

# need kcmshell4 from kde-runtime at least
Requires: kde-runtime%{?_kde4_version: >= %{_kde4_version}}
# not *required*, but very nice.  allows support for instant changes, gtk3.
Requires: xsettings-kde

%description
This is a System Settings configuration module for configuring the
appearance of GTK apps in KDE.

%prep
%setup -q 

%patch50 -p1 -b .settings_category
%patch51 -p1 -b .gtkrc_setenv
%patch52 -p1 -b .fix-de
%patch53 -p1 -b .xsettings_kipc

# fixup for kde-4.5+, see http://bugzilla.redhat.com/628381
sed -i.kde45 -e 's|^X-KDE-System-Settings-Parent-Category=appearance$|X-KDE-System-Settings-Parent-Category=application-appearance|' \
  kcmgtk.desktop


%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang kcm_gtk


%files -f kcm_gtk.lang
%doc Changelog COPYING
%{_kde4_libdir}/kde4/kcm_gtk.so
%{_kde4_iconsdir}/kcm_gtk.png
%{_kde4_datadir}/kde4/services/kcmgtk.desktop


%changelog
* Sat May 06 2017 Sérgio Basto <sergio@serjux.com> - 1:0.5.3-21
- Add Epoch 1 to not be obsoleted by kde-gtk-config

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-20
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-19
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-18
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 0.5.3-17
- Rebuilt for GCC 5 C++11 ABI change

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-16
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-15
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Aug 14 2013 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-14
- Requires: xsettings-kde

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon May 14 2012 Rex Dieter <rdieter@fedoraproject.org>
- 0.5.3-10
- update kcm_category patch
- kubuntu_01_xsettings_kipc.patch
- drop old Obsoletes: gtk-qt-engine

* Tue Jan 17 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.5.3-9
- drop gtk3 patch again, the new plan is to handle this through xsettings-kde

* Fri Jan 06 2012 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.5.3-8
- add support for GTK+ 3 (backported from upstream bzr gtk3 branch)
- drop ancient Fedora < 13 env_script conditional, now always in kde-settings

* Mon Mar 14 2011 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.5.3-7
- drop cursortheme patch, now set automatically by xsettings-kde (#591746)

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> 0.5.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Sep 08 2010 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-5
- kcm-gtk : "GTK+ Appearance" in systemsettings->lost and found (#628381)
- Requires: kdebase-runtime

* Wed Jul  7 2010 Ville Skyttä <ville.skytta@iki.fi> 0.5.3-4
- Apply modified upstream patch to add cursor theme support (#600976).

* Fri Dec 25 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-3
- GTK2_RC_FILES handling moved to kde-settings (#547700)

* Sun Dec 20 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.5.3-2
- fix missing umlauts and sharp s in the German translation

* Fri Oct 30 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.3-1
- kcm-gtk-0.5.3
- .gtkrc-2.0-kde4 doesn't get used (#531788)

* Thu Oct 22 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.1-2
- Requires: kde4-macros(api)...

* Thu Oct 22 2009 Rex Dieter <rdieter@fedoraproject.org> 0.5.1-1
- kcm-gtk-0.5.1 (first try)

