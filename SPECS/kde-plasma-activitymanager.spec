Name:           kde-plasma-activitymanager
Version:        0.6.2
Release:        1%{?dist}
Summary:        KDE plasma activity manager applet

Group:          User Interface/Desktops
License:        GPLv2
URL:            http://kde-apps.org/content/show.php?content=136278
Source0:        https://dl.opendesktop.org/api/files/download/id/1460733240/148508-activitymanager-wheeled-%{version}.tar.gz
BuildRequires:  kdeplasma4-addons-devel

%{?_kde4_macros_api:Requires: kde4-macros(api) = %{_kde4_macros_api} }

%description
Lightweight plasmoid to manage your activities effectively.
Can start, stop, switch between, clone, edit and remove activities.

%prep
%setup -q -n activitymanager-wheeled-%{version}


%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=$RPM_BUILD_ROOT -C %{_target_platform}


%files
%doc COPYING ChangeLog
%{_kde4_libdir}/kde4/plasma_applet_activitymanager.so
%{_kde4_datadir}/kde4/services/plasma-applet-activitymanager.desktop


%changelog
* Tue Nov 29 2016 Sérgio Basto <sergio@serjux.com> - 0.6.2-1
- Update to activitymanager-wheeled-0.6.2

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Feb 21 2011 Magnus Tuominen <magnus.tuominen@gmail.com> - 0.5-2
- BR corrected
- R removed
- desktop-file-utils not needed
- renamed

* Sun Feb 20 2011 Magnus Tuominen <magnus.tuominen@gmail.com> - 0.5-1
- first build
