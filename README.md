# kde4-fedora

This repo is a base for the following project : 

https://copr.fedoraproject.org/coprs/peem/kde4-fedora/

usefull git/rpm aliases

    alias rpmbuild='rpmbuild -bs'    
    alias obsoletes='rpmspec -q --obsoletes'    
    alias provides='rpmspec -q --provides'   
    alias requires='rpmspec -q --requires'  
    alias add='git add'  
    alias commit='git commit'  
    alias push='git push'  
    alias status='git status'
    alias fetchsrc='spectool -C SOURCES -g'

Install rpmdevtools and use spectool to fetch sources for spec : 

    dnf install rpmdevtools -y

    spectool -C SOURCES -g SPECS/kde-print-manager.spec

NOTE : Useful git tool : 
    https://rtyley.github.io/bfg-repo-cleaner/

git commit flow when submodule is updated : 

    cd submodule/
    git add -a
    git commit
    git push origin HEAD:master
    cd ..
    git add submodule
    git commit
    git push